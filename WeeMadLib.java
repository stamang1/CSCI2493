package gui.bin;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import edu.cmu.ri.createlab.terk.robot.finch.Finch;

class WeeMadLib implements ActionListener {
	
	
	Finch myFinch = new Finch();
	
	JFrame wmlGame = new JFrame("Wee Mad Lib");
	JPanel gameBoard = new JPanel(new GridLayout(3,3));

	JButton RunForward = new JButton("Run Forward");
	JButton ReverseBackWard = new JButton("Reverse BackWard");
	JButton Stop = new JButton("stop");

	

	JButton clear = new JButton("Clear");
	JTextArea sentence = new JTextArea("Changing Color ___ ___.");
	JButton exit = new JButton("Exit");

	public WeeMadLib() {

		wmlGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		wmlGame.setSize(300,300);

		RunForward.setOpaque(true);
		RunForward.setBackground(new Color(0,255,255));
		RunForward.setHorizontalAlignment(JLabel.CENTER);

		sentence.setEditable(false);
		sentence.setOpaque(false);
		sentence.setLineWrap(true);
		sentence.setWrapStyleWord(true);

		exit.addActionListener(this);
		clear.addActionListener(this);
		
		
		RunForward.addActionListener(this);
		ReverseBackWard.addActionListener(this);
		Stop.addActionListener(this);

		gameBoard.add(RunForward);
		gameBoard.add(ReverseBackWard);		
		gameBoard.add(Stop);
		gameBoard.add(clear);
		gameBoard.add(exit);


		wmlGame.setContentPane(gameBoard);
		wmlGame.setVisible(true);
		wmlGame.pack();
	}

	public void actionPerformed(ActionEvent e) {
		
		

		if (e.getSource() == exit) {

			wmlGame.dispose();

		} else if (e.getSource() == clear) {

			sentence.setText("");

		}

		else if (e.getSource() == RunForward) {
			SwitchCase(1);
		}
		else if (e.getSource() == ReverseBackWard) {
			SwitchCase(2);	
		}
		else if (e.getSource() == Stop) {
			SwitchCase(5);
		}

	}

	public static void main(String[] args) {

		new WeeMadLib();

	}
	
	
	public void SwitchCase(int caseNumber){
		switch (caseNumber) {
		    case 1: {
		   	 myFinch.saySomething("Moving to the Forward Direction");
		   	 myFinch.setLED(0, 255, 255);
		   	 myFinch.setWheelVelocities(255,255,3000);
		   	System.out.println("Working");
		   	 break;
		   	 }
		    
		    case 2: {
		   	 myFinch.saySomething("Moving to the Backward Direction");
		   	 myFinch.setLED(0, 0, 255);
		   	 myFinch.setWheelVelocities(-255,-255,3000);
		   	System.out.println("Working");
		   	break; 
		    }
		    
		    case 3:  {
		   	 myFinch.saySomething("Moving to the Left Direction");
		   	 myFinch.setLED(0, 255, 0);
		   	 myFinch.setWheelVelocities(0,255,100000);
		   	System.out.println("Working");
		   
		    	}
		    
		    case 4: {
		   	 myFinch.saySomething("Moving to the Right Direction");
		   	 myFinch.setLED(255, 0, 0);
		   	 myFinch.setWheelVelocities(255,0,100000);
		   	 System.out.println("Working");
		   	 
		    	}
		    
		    case 5: {
		    myFinch.saySomething("Stoping the Finch");
			 myFinch.setLED(0, 255, 0);
			 myFinch.setWheelVelocities(0,0);
			 System.out.println("Working");
			 break;
		    }
	    }


	}
}
