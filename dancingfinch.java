package Dancing.finch;

/**
 * Created by: Suman Shrestha,Suman Tamang ,Sangita Acharya ,Bibek Karki
 * Date: 10/21/2019
 * Simple program to have the Finch do a dance with the song called ' i always feel like somebody is watching me '
 */

import edu.cmu.ri.createlab.terk.robot.finch.Finch;

public class dancingfinch 
{

   public static void main(final String[] args)
   {
	  // Instantiating the Finch object
      Finch myFinch = new Finch();
      
      
      
      myFinch.saySomething("Time to do a little dance !");
      
      System.out.println("Time to do a little dance ");
      // Set LED green, and move forward full speed for one second
      
      
      for (int i=0; i<5; i++) {
      myFinch.setLED(0, 255, 0);
	      myFinch.setWheelVelocities(180,180,500);
	      // Set LED yellow and turn for a half second
	      myFinch.setLED(255, 255, 0);
	      myFinch.setWheelVelocities(-180,-180,500);
	      
	      myFinch.setLED(0, 255, 0);
 	      myFinch.setWheelVelocities(150,150,500);
 	      // Set LED yellow and turn for a half second
 	      myFinch.setLED(255, 255, 0);
 	      myFinch.setWheelVelocities(180,180,500);
 	      // Set LED Red and back up for a second
      
 	     myFinch.setWheelVelocities(0, 250, 5000);
 	      //make a finch turn left for 5 second
 	      
 	      myFinch.setWheelVelocities(250, 0, 5000);
 	      
 	     myFinch.setWheelVelocities(0, 250, 5000);
 	     
 	     myFinch.setWheelVelocities(-250, -250, 500);
 	      //make a finch turn left for 5 second
 	      
 	      myFinch.setWheelVelocities(250, 250, 5000);
 	     }
      myFinch.quit();
      System.exit(0);
    
}
}